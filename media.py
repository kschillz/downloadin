
class Media(object):
    def __init__(self, id_, name):
        self.id = id_
        self.name = name

    def __str__(self):
        return '<{}: {}>'.format(str(type(self).__name__), str(self.__dict__))


class Series(Media):
    def __init__(self, id_, name, episodes):
        super().__init__(id_, name)
        self.episodes = episodes


class Movie(Media):
    def __init__(self, id_, name, url):
        super().__init__(id_, name)
        self.url = url


class Episode(Media):
    def __init__(self, id_, name, url):
        super().__init__(id_, name)
        self.url = url

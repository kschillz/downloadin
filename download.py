import requests
from progressbar import (ProgressBar,
                         Bar,
                         Percentage,
                         FileTransferSpeed,
                         ETA,
                         DataSize)
from os import makedirs
from pprint import pprint
from pyquery import PyQuery as pq
from pyaria2 import PyAria2, isAria2Installed
import time


def download_to_file(media, path):
    if isAria2Installed():
        return download_aria2(media, path)
    else:
        return download_python_stream(media, path)


def download_python_stream(media, path):
    r = requests.get(media.url, stream=True)

    filename = media.name.replace(' ', '.').replace(':', '').replace('-', '')
    filename += '.mp4'
    # filename = (r.headers['Content-Disposition'].split('=')[-1].strip('"-')
    #             if r.headers['Content-Disposition']
    #             else media.url.split('/')[-1])
    filesize = int(r.headers['Content-Length'])

    # makedirs(path)

    print('Downloading {} to {}...'.format(media.url, path + filename))

    widgets = [
        filename, ": ", DataSize(), ' of ', str(round(filesize/1048576, 1))+' MiB',
        Bar(), " ", Percentage(), " ",  FileTransferSpeed(), " ", ETA()
    ]

    with open(path + filename, 'wb') as f:
        with ProgressBar(widgets=widgets, max_value=filesize) as p:
            b = 0
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    b += len(chunk)
                    p.update(b)
    print('Downloaded {}'.format(filename))
    return filename


def download_aria2(media, path):
    filename = media.name.replace(' ', '.').replace(':', '').replace('-', '')
    filename += '.mp4'

    aria = PyAria2()
    gid = aria.addUri([media.url], {
        'dir': path,
        'out': filename
    })
    status = aria.tellStatus(gid)
    while status['status'] != 'complete':
        del status['files']
        pprint(status)
        time.sleep(3)
        status = aria.tellStatus(gid)
    pprint(status)

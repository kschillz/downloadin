import argparse
import configparser
from pprint import pprint
from superchillin import Superchillin
from download import *


def download_media(media, path):
    return download_to_file(media, path)


def download_series(series, path):
    filenames = []
    for episode in series:
        filename = download_media(episode, path)
        filenames.append(filename)
    return filenames


def choose_search_result(results):
    print('Movies:')
    movie_choices = {
        index: movie
        for index, movie in enumerate(results['movies'].keys())
    }
    for key, value in movie_choices.items():
        print('\t{}: {}'.format(key, value))
    print('\nTV Shows:')
    series_choices = {
        index + len(movie_choices): series
        for index, series in enumerate(results['series'].keys())
    }
    for key, value in series_choices.items():
        print('\t{}: {}'.format(key, value))

    confirm = 'n'
    while confirm == 'n':
        choice = int(input('Choose a number: '))
        title = (movie_choices[choice]
                 if choice in movie_choices
                 else series_choices[choice])
        confirm = input('Download {}? [y/n/q]'.format(title))
        if 'y' in confirm:
            if choice in movie_choices:
                return 'm', results['movies'][title]
            elif choice in series_choices:
                return 's', results['series'][title]
        elif 'q' in confirm:
            break
        else:
            continue


def main():
    args = argparse.ArgumentParser()
    group = args.add_mutually_exclusive_group()
    group.add_argument('-m', '--movies', action='store', type=str,
                       nargs='+', help="List of movie IDs to download.")
    group.add_argument('-t', '--tv_series', action='store', type=str,
                       nargs='+', help="List of TV show IDs to download.")
    group.add_argument('-e', '--episodes', action='store', type=str,
                       nargs='+', help="List of episode IDs to download.")
    group.add_argument('-s', '--search', action='store', type=str,
                       help="Search input string and download first result.")
    args.add_argument('-p', '--path', action='store', type=str,
                      help="Path to download file(s) to.")
    args.add_argument('-n', action='store_true',
                      help='Skip actual download for testing.')
    args.add_argument('-v', action='store_true',
                      help='verbose')
    argv = args.parse_args()

    config = configparser.ConfigParser()
    config.read('config.ini')

    s = Superchillin(config['superchillin']['email'],
                     config['superchillin']['password'])

    path = argv.path if argv.path is not None else ''
    if path is not '':
        if not path.endswith('/'):
            path += '/'

    if argv.movies:
        for movie in argv.movies:
            m = s.movie(movie)
            if not argv.n:
                download_media(m, path)

    if argv.episodes:
        for episode in argv.episodes:
            e = s.episode(episode)
            if not argv.n:
                download_media(e, path)

    if argv.tv_series:
        for series in argv.tv_series:
            s = s.series(series)
            if not argv.n:
                download_series(s, path)

    if argv.search:
        results = s.search(argv.search)
        media_type, media_id = choose_search_result(results)
        if media_type == 'm':
            movie = s.movie(media_id)
            if not argv.n:
                download_media(movie, path)
        elif media_type == 's':
            series = s.series(media_id)
            if not argv.n:
                download_series(series, path)


if __name__ == '__main__':
    main()

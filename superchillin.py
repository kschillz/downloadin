import random
import re
import requests

from html import unescape
from pprint import pprint
from download import *
from media import *


class Superchillin(object):
    """
    Handles logging into and navigating Superchillin
    """
    BASE = 'http://superchillin.com'
    MOVIE = BASE + '/?{}'
    EPISODE = BASE + '/?{}&tv=1'
    SERIES = BASE + '/episodes.php?{}'
    SEARCH = BASE + '/search.php?q={}'
    LOGIN = BASE + '/login2.php'
    SERVERS = {
        'Dallas Free': '22',
        'Frankfurt Free': '24',
        'Global CDN': '8',
        'London': '10',
        'Amsterdam': '18',
        'Copenhagen': '20',
        'Frankfurt Premium': '30',
        'Paris': '36',
        'New York City': '38',
        'Washington D.C.': '42',
        'Chicago': '46',
        'Seattle': '50',
        'Dallas Premium': '52',
        'Los Angeles': '62',
        'San Jose': '65',
        'Miami': '72',
        'Toronto': '76',
        'Montreal': '78',
        'Sydney': '80'
    }
    REVERSE_SERVERS = {v: k for k, v in SERVERS.items()}

    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.session = self.login()

    def login(self):
        session = requests.Session()
        login = session.post(self.LOGIN, data={
            'email': self.email,
            'password': self.password
        })
        if 'invalid' in login.url:
            raise
        return session

    def get_links(self, url, html):
        page = pq(html)
        hd_link = page('a[href$="&hd=1"]')
        if hd_link:
            print('Found HD download!')
            hd_url = url + hd_link[0].attrib['href']
            page = pq(self.session.get(hd_url)._content)
        print('Finding link(s)...')
        mp4_links = page('a[href$="mp4"]')
        dl_links = [link.attrib['href']
                    for link in mp4_links
                    if 'dl' not in link.attrib['href']]

        return dl_links

    def get_media_ids(self, html):
        page = pq(html)
        print('Finding IDs on search page...')
        movie_links = page('a.tippable')
        movies = {}
        for link in movie_links:
            key = '{}-{}'.format(link.text, link.attrib['id'])
            movies[key] = link.attrib['href'].split('?')[-1]

        series_links = page('a[href^="/episodes"]')
        series = {}
        for link in series_links:
            id_ = link.attrib['href'].split('?')[-1]
            key = '{}-{}'.format(link.text, id_)
            series[key] = id_

        return {
            'movies': movies,
            'series': series
        }

    def get_best_server(self):
        print('Finding best server...')
        page = pq(self.session.get(self.BASE)._content)
        usage = page('span[title*="different location"]')
        usage_list = [
            use.text.strip('%')
            for use in usage
        ]
        servers = [
            server.attrib['href'].split('=')[-1]
            for server in usage.siblings('span a.hoverz')
        ]
        server_usage = dict(zip(servers, usage_list))
        best = min(server_usage.items(), key=lambda x: int(x[1]))
        print('Best server: {}, {}%'.format(
            self.REVERSE_SERVERS[best[0]], best[1]))
        return best[0]

    def get_media_name(self, html):
        page = pq(html)
        name = page('div[style*="position:absolute; left:20px; bottom:6px"]')
        return unescape(name.text())

    def get_episodes(self, html):
        page = pq(html)
        episodes = page('a[href$="&tv=1"]')
        return [episode.attrib['href'] for episode in episodes]

    def movie(self, superchillin_id):
        movie_url = self.MOVIE.format(superchillin_id)
        print('GET: {}'.format(movie_url))
        movie_page = self.session.get(movie_url)
        dl_url = self.BASE + self.get_links(movie_url, movie_page._content)[0]
        name = self.get_media_name(movie_page._content)
        m = Movie(superchillin_id, name, dl_url)
        return m

    def episode(self, superchillin_id):
        episode_url = self.EPISODE.format(superchillin_id)
        print('GET: {}'.format(episode_url))
        episode_page = self.session.get(episode_url)
        dl_url = self.BASE + self.get_links(episode_url, episode_page._content)[0]
        name = self.get_media_name(episode_page._content)
        e = Episode(superchillin_id, name, dl_url)
        return e

    def series(self, superchillin_id):
        series_url = self.SERIES.format(superchillin_id)
        print('GET: {}'.format(series_url))
        series_page = self.session.get(series_url)
        episode_links = self.get_episodes(series_page._content)
        episodes = []
        for ep in episode_links:
            ep_id = re.findall(r'\d+', ep)[0]
            episodes.append(self.episode(ep_id))
        return episodes

    def search(self, query):
        query = query.replace(' ', '+')
        search_page = self.session.get(self.SEARCH.format(query))
        return self.get_media_ids(search_page._content)
